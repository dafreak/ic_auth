<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title><?=$title ?></title>
		<link type="text/css" href="/css/style.css" rel="stylesheet" /><?
		if (isset($javascript)) {
			foreach ($javascript as $key => $value) {
				echo "\n\t\t" . '<script type="text/javascript" src="/js/' . $value . '"></script>';
			}
		}
		if (isset($css)) {
			foreach ($css as $key => $value) {
				echo "\n\t\t" . '<link type="text/css" href="/css/' . $value . '" rel="stylesheet" />';
			}
		}
		echo "\n";
		?>
	</head>
	<body>
    <?=modules::run('auth/ic_auth_widgets/cp',isset($hide_cp),$this->uri->uri_string());?>