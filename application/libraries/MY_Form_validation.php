<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * Form validation extender
 *
 * This class extends CI_Form_validation so so that form validaion can use
 * external callbacks from models or libraries and unset _field_data
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		IC
 * @version 	10-13-2012 22:47:23
 */
class MY_Form_validation extends CI_Form_validation {
	
	public $CI;

	/**
	 * Generic callback used to call callback methods for form validation.
	 *
	 * @param  string  the value to be validated
	 * @param  string  a comma separated string that contains the model name,
	 *                 method name and any optional values to send to the method
	 *                 as a single parameter.
	 *
	 *                 - First value is the external class type (library or model)
	 *                 - Second value is the name of the class.
	 *                 - Third value is the name of the method.
	 *                 - Any additional values are values to be
	 *                   sent in an array to the method.
	 *
	 *  EXAMPLE RULE FOR CALLBACK IN MODEL:
	 *  external_callbacks[model,model_name,some_method,some_string,another_string]
	 *
	 *  EXAMPLE RULE FOR CALLBACK IN LIBRARY:
	 *  external_callbacks[library,library_name,some_method,some_string,another_string]
	 *
	 *  NOTE: the callback function set_message should refer to "external_callbacks"
	 *  $this->form_validation->set_message('external_callbacks', 'Message');
	 *
	 */
	public function external_callbacks($postdata, $param) {
		$param_values = explode(',', $param);

		// Load the model or library holding the callback
		$class_type = $param_values[0];
		$class_name = $param_values[1];
		$this->CI->load->$class_type($class_name);

		// Apply method name to variable for easy usage
		$method = $param_values[2];

		// Check to see if there are any additional values to send as an array
		if (count($param_values) > 3) {
			// Remove the first three elements in the param_values array
			$argument = array_slice($param_values, 3);
		}

		// Do the actual validation in the external callback
		if (isset($argument)) {
			$callback_result = $this->CI->$class_name->$method($postdata, $argument);
		} else {
			$callback_result = $this->CI->$class_name->$method($postdata);
		}

		return $callback_result;
	}

	// ------------------------------------------------------------------------

	/**
	 * Unsets any field data set
	 *
	 * Unsetsthe _field_data array then recreates it
	 *
	 * @access	public
	 * @return	NULL
	 */
	function unset_field_data() {
		unset($this->_field_data);
		$this->_field_data = array();
		log_message('debug', "Form Validation Field Data Unset");
	}

	// ------------------------------------------------------------------------

}

// END MY_Form_validation class

/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */
