<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Error Controller
 * --------------------------------------
 * Author       : IC
 * Revision     : 1.0
 * Date         : 10-10-2012
 * Position     :
 *
 */

class Error extends MX_Controller {

    function __construct() {
        $this->load->language('error/error');
        parent::__construct();

    }

    public function index() {
        $data['page'] = 'error_view';
        $title = $this->session->flashdata('error_title');
        $message = $this->session->flashdata('error_message');
        if ($title == NULL) {
            $title = $this->lang->line('default_title');
        }
        if ($message == NULL) {
            $message = $this->lang->line('default_message');
        }
        $data['title'] = $title;
        $data['message'] = $message;

        $this->load->view('error/template/template_view', $data);

    }

}
