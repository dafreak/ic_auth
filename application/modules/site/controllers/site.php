<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/* Author: IC
 * Description: Home controller class
 * This is only viewable to those members that are logged in
 */
class Site extends MX_Controller {
    function __construct() {
        parent::__construct();
         $this->load->language('site/site');
		// $this->load->library('auth/ic_auth');
        // $this->ic_auth->check_is_logged_in();
    }

    public function index() {
        
        $data['page'] = 'home_view';
        $data['title'] = $this->lang->line('title_' . $data['page']);
        $this->load->view('site/template/template_view', $data);

    }

}
?>
