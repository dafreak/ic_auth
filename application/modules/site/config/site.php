<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| AUTH SETTINGS 
| -------------------------------------------------------------------
| This file will contain the settings needed to use the Auth addon
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
| Auth uses a basic users table with includes password pepper and the 
| encryption_key in the config (application/config/config.php) which 
| needs to be changed for per install as it will provide greater security 
| 
| For random Salt try https://api.wordpress.org/secret-key/1.1/salt/ 
|
| Rebuild Databas           application/sql/rebuild_user_example.sql
| Basic Database Setup      application/sql/new_database.sql
| Basic USERS Table SQL     application/sql/users.sql
|
| To access values stored in this config
| $this->config->item('<key_name>', 'auth');
|
*/

/*   
|--------------------------------------------------------------------------
| Site settions
|--------------------------------------------------------------------------
|
| Used to set parameters for the site
|
*/
$config['allow_signup']          = TRUE; 


/* 
|--------------------------------------------------------------------------
| Admin user defaults
|--------------------------------------------------------------------------
|
| Used to set parameters for the admin accoutn creation
|
*/
$config['admin_username']		='admin';
$config['admin_password']		='password';
$config['admin_email']			='admin@localhost.com';


/*	
|--------------------------------------------------------------------------
| Site Infomation
|--------------------------------------------------------------------------
|
| Used to set parameters for the site
|
*/
$config['site_title']			='User Authentication Example'; 
$config['user_expire']          = 86500; 				// How long to remember the user (seconds). Set to zero for no expiration
$config['email_from']			='administrator@no-reply.com';
$config['email_from_name'] 		= 'Administrator';

/* End of file auth.php */
/* Location: ./application/config/auth.php */