<? //Setup form values
$label = array('username' => 'Enter Username', 'email' => 'Enter valid email address', 'password' => 'New Password', 'password2' => 'Password Confrimation');
$username = array('name' => 'username', 'id' => 'username');
$password = array('name' => 'password', 'id' => 'password');
$password2 = array('name' => 'password2', 'id' => 'password2');
$email = array('name' => 'email', 'id' => 'email');
?>
		<h1>Reset Password</h1>

		<fieldset>
			<legend>
				Login Info
			</legend>
			<?=form_open('auth/reset_password'); ?>
			<?=form_label($label['password'], $password['name']); ?>
			<?=form_password($password); ?>
			<br/>
			<?=form_label($label['password2'], $password2['name']); ?>
			<?=form_password($password2); ?>
			<?=form_hidden('s_validation_code', $s_validation_code); ?>
			<?=form_hidden('s_userid',$s_userid); ?>

			<?=form_submit('submit', 'Update password'); ?>
			<?=form_close(); ?>
			<?php
            if (!is_null($message))
                echo '<div class="message">' . $message . '</div>';
			?>
			<?php echo validation_errors('<p class="error">'); ?>
		</fieldset>
