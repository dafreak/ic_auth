<? //Setup form values
$formdata['username']['label'] = 'Enter Username';
$formdata['username']['data'] = array('name' => 'username', 'id' => 'username');
$formdata['username']['type'] = 'form_input';

$formdata['email2']['label'] = 'Enter valid email address';
$formdata['email2']['data'] = array('name' => 'email2', 'id' => 'email2', );
$formdata['email2']['type'] = 'form_input';

$formdata['email']['label'] = 'Enter valid email address';
$formdata['email']['data'] = array('name' => 'email', 'id' => 'email', );
$formdata['email']['type'] = 'form_input';

$formdata['password']['label'] = 'Password';
$formdata['password']['data'] = array('name' => 'password', 'id' => 'password');
$formdata['password']['type'] = 'form_password';

$formdata['password2']['label'] = 'Password Confrimation';
$formdata['password2']['data'] = array('name' => 'password2', 'id' => 'password2');
$formdata['password2']['type'] = 'form_password';
?>
<h2>Create an Account</h2>
<fieldset>
	<legend>
		Account Information
	</legend>
	<?=form_open('auth/signup'); ?>
	<?php
	foreach ($formdata as $field) {
		if ($field['type'] != 'form_hidden') {
			echo form_label($field['label'], $field['data']['name']);
			if ($field['type'] == 'form_password') {
				echo $field['type']($field['data']);
			} else {
				echo $field['type']($field['data'], set_value($field['data']['name']));
			}
			echo "<br>\n\r";
		} else {
			echo $field['type']($field['data']['name'], $field['data']['value']);
		}
	}
	?>
	<?=form_submit('submit', 'Sign Up'); ?>
	<?=form_close(); ?>
	<?php
	if (!is_null($message))
		echo '<div class="message">' . $message . '</div>';
	?>
	<?php echo validation_errors('<p class="error">'); ?>
</fieldset>

</p>
