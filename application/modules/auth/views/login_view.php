<? //Setup form values
$label = array('username' => 'Enter Username', 'password' => 'Password', 'remember' => 'Remember Me');
$username = array('name' => 'username', 'id' => 'username');
$password = array('name' => 'password', 'id' => 'password');
$remember = array('name' => 'remember', 'id' => 'remember');
?>
<div id="login_form">
		<h2>User Login</h2>
    	<fieldset>
			<legend>
				Login Info
			</legend>
			<?=form_open('auth'); ?>
			<?=form_label($label['username'], $username['name']); ?>
			<?=form_input($username, set_value('username')); ?>
			<br/>
			<?=form_label($label['password'], $password['name']); ?>
			<?=form_password($password); ?>
			<br>
			<?=form_label($label['remember'], $remember['name']); ?>
			<?=form_checkbox($remember, 'remember') ?>
			<br/>
			<?=form_submit('submit', 'Login'); ?>
			<?=form_close(); ?>
			<?php
            if (!is_null($message))
                echo '<div class="message">' . $message . '</div>';
			?>
			<?php echo validation_errors('<p class="error">'); ?>
		</fieldset>
		<? echo '<a href="' . site_url('auth/signup').'">Sign Up</a>'
		?>
		&nbsp;&nbsp;|&nbsp;&nbsp;
		<? echo '<a href="' . site_url('auth/forgot').'">Forgot Username or Password </a>'
		?>
</div>
