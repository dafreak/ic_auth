<? //Setup form values
$label = array('username' => 'Enter Username', 'password' => 'Password', 'remember' => 'Remember Me');
$username = array('name' => 'username', 'id' => 'username');
$password = array('name' => 'password', 'id' => 'password');
$remember = array('name' => 'remember', 'id' => 'remember');
?>
<div class="control_panel">
	<?=form_open('auth'); ?>
	<?=form_label($label['username'], $username['name']); ?>
	<?=form_input($username, set_value('username')); ?>
	<?=form_label($label['password'], $password['name']); ?>
	<?=form_password($password); ?>
	<?=form_label($label['remember'], $remember['name']); ?>
	<?=form_checkbox($remember, 'remember') ?>
	<?=form_hidden('redirect_page', $redirect_page) ?>
	<?=form_submit('submit', 'Login'); ?>

	<? echo '<a href="' . site_url('auth/signup').'">Sign Up</a>'
	?>
	&nbsp;&nbsp;|&nbsp;&nbsp;
	<? echo '<a href="' . site_url('auth/forgot').'">Forgot Username or Password </a>'
	?><?=form_close(); ?>
</div>