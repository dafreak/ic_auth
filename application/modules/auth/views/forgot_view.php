<? //Setup form values
$label = array('email' => 'Enter Email Address');
$email = array('name' => 'email', 'id' => 'email');
?>

<h1>Retrive Account Information</h1>
<fieldset>
	<legend>
		Account Information
	</legend>

	To recover your username or password, type the full email address you used to sign up
	<br/>
	<?=form_open('auth/forgot'); ?>
	<?=form_label($label['email'], $email['name']); ?>
	<?=form_input($email); ?>
	<?=form_submit('submit', 'Send Information'); ?>
	<?=form_close(); ?>
	<?php echo validation_errors('<p class="error">'); ?>
	<?php
	if (isset($message))
		echo '<div class="message"' . $message . '</div>';
	?>
</fieldset>

</p>

