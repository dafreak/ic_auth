<?
$label = array('username' => 'Enter Username', 'current_password' => 'Enter Current Password', 'password' => 'New Password', 'password2' => 'Password Confrimation');
$current_password = array('name' => 'current_password', 'id' => 'current_password');
$password = array('name' => 'password', 'id' => 'password');
$password2 = array('name' => 'password2', 'id' => 'password2');

?>
		<h1>Change Password</h1>

		<fieldset>
			<legend>
				Login Info
			</legend>
			
			<?=form_open('auth/change_password'); ?>
			<?=form_label($label['current_password'], $current_password['name']); ?>
			<?=form_password($current_password); ?>
			<br/>
			<?=form_label($label['password'], $password['name']); ?>
			<?=form_password($password); ?>
			<br/>
			<?=form_label($label['password2'], $password2['name']); ?>
			<?=form_password($password2); ?>

			<?=form_submit('submit', 'Change password'); ?>
			<?=form_close(); ?>
			
			<?php echo validation_errors('<p class="error">'); ?>
			<?php
	if (isset($message))
		echo '<div class="message"' . $message . '</div>';
	?>
		</fieldset>
