<?
$label = array( 'new_email' => 'Enter valid email address','new_email2' => 'Confirm email address');
$new_email = array('name' => 'new_email', 'id' => 'new_email');
$new_email2 = array('name' => 'new_email2', 'id' => 'new_email2')
?>
<h1>Change Email</h1>
<fieldset>
	<legend>
		Account Information
	</legend>
	<?=form_open('auth/change_email'); ?>
	<?=form_label($label['new_email'], $new_email['name']); ?>
	<?=form_input($new_email, set_value('new_email')); ?>
	<br/>
	<?=form_label($label['new_email2'], $new_email2['name']); ?>
	<?=form_input($new_email2, set_value('new_email2')); ?>
	<br/>
	<?=form_submit('submit', 'Change Email'); ?>
	<?=form_close(); ?>

	<?php echo validation_errors('<p class="error">'); ?>
	<?php
	if (isset($message))
		echo '<div class="message"' . $message . '</div>';
	?>
</fieldset>