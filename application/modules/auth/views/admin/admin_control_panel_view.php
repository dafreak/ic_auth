<p><a href="<?php echo site_url('auth/admin/add_user'); ?>">Create a new user</a></p>
<table cellpadding=0 cellspacing=10 width="750px">
	    <tr>
    	<td colspan="6"><div style="margin: auto;text-align: center ;"><?=$pagination_links; ?></div></td>
    </tr>
    <tr>
        <th>UserID</th>
        <th>User Name</th>
        <th>Email</th>
        <th>Status</th>
        <th colspan="2">Actions</th>
    </tr>
    
    <?php foreach ($users as $user):?>
        <tr>
            <td><?=$user->userid; ?></td>
            <td><?=$user->username; ?></td>
            <td><?=$user->email; ?></td>
            <td><?=$user->email_validated; ?></td>
            <td><? echo '<a href="' . site_url('auth/admin/edit_user/' . $user->userid).'">Edit</a>'; ?></td>
            <td><? echo '<a href="' . site_url('auth/admin/delete_user/' . $user->userid) . '">Delete</a>'; ?></td>
        </tr>
    <?php endforeach; ?>
    <tr>
    	<td colspan="6"><div style="margin: auto;text-align: center ;"><?=$pagination_links; ?></div></td>
    </tr>
</table>
<?php
	if (isset($message))
		echo '<div class="message"' . $message . '</div>';
	?>
