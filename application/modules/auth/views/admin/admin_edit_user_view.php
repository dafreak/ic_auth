<? //Setup form values
$formdata['username']['label'] = 'Enter Username';
$formdata['username']['data'] = array('name' => 'username', 'id' => 'username', 'value' => $user['username'], 'readonly' => 'readonly');
$formdata['username']['type'] = 'form_input';

$formdata['email2']['label'] = 'Enter valid email address';
$formdata['email2']['data'] = array('name' => 'email2', 'id' => 'email2', 'value' => $user['email']);
$formdata['email2']['type'] = 'form_input';

$formdata['email']['label'] = 'Enter valid email address';
$formdata['email']['data'] = array('name' => 'email', 'id' => 'email', 'value' => $user['email']);
$formdata['email']['type'] = 'form_input';

$formdata['password']['label'] = 'Password';
$formdata['password']['data'] = array('name' => 'password', 'id' => 'password');
$formdata['password']['type'] = 'form_password';

$formdata['password2']['label'] = 'Password Confrimation';
$formdata['password2']['data'] = array('name' => 'password2', 'id' => 'password2');
$formdata['password2']['type'] = 'form_password';

$formdata['email_validated']['label'] = 'Users Email address is valid';
$formdata['email_validated']['data'] = array('name' => 'email_validated', 'id' => 'email_validated', 'checked' => $user['email_validated'], 'value' => '1');
$formdata['email_validated']['type'] = 'form_checkbox';

$formdata['send_email_validation']['label'] = 'Send Validation Email';
$formdata['send_email_validation']['data'] = array('name' => 'send_email_validation', 'id' => 'send_email_validation', 'value' => '1');
$formdata['send_email_validation']['type'] = 'form_checkbox';

$formdata['admin']['label'] = 'Is User Account an Admin account';
$formdata['admin']['data'] = array('name' => 'admin', 'id' => 'admin', 'checked' => $user['admin'], 'value' => '1');
$formdata['admin']['type'] = 'form_checkbox';

$formdata['userid']['label'] = 'Enter Username';
$formdata['userid']['data'] = array('name' => 'userid', 'value' => $user['userid'], );
$formdata['userid']['type'] = 'form_hidden';
?>
<h2>Edit an Account</h2>
<fieldset>
	<legend>
		Update Account Information
	</legend>
	<?=form_open('auth/admin/edit_user'); ?>
	<?php
	foreach ($formdata as $field) {
		if ($field['type'] != 'form_hidden') {
			echo form_label($field['label'], $field['data']['name']);
			if ($field['type'] == 'form_password') {
				echo $field['type']($field['data']);
			} else {
				echo $field['type']($field['data'], set_value($field['data']['name']));
			}
			echo "<br>\n\r";
		} else {
			echo $field['type']($field['data']['name'], $field['data']['value']);
		}
	}
	?>
	<?=form_submit('submit', 'Update Acccount'); ?>
	<?=form_close(); ?>
	<?php
	if (isset($message))
		echo '<div class="message">' . $message . '</div>';
	?>
	<?php echo validation_errors('<p class="error">'); ?>
</fieldset>

</p>
