/* Clean up OLD database*/
USE user_example;
DROP USER 'user_example'@'localhost' ;
DROP database user_example;


/* Create NEW database*/
Create database user_example; 
USE user_example;
CREATE USER 'user_example'@'localhost' IDENTIFIED BY 'user_example';
GRANT ALL ON user_example.* TO 'user_example'@'localhost';


CREATE TABLE users
(
  userid Int NOT NULL AUTO_INCREMENT,
  username Char(20) NOT NULL,
  password Char(255) NOT NULL,
  password_pepper Char(255) NOT NULL,
  email Char(255) NOT NULL,
  email_validated Bool NOT NULL DEFAULT 0,
  new_email Char(255),
  new_email_validated Bool NOT NULL DEFAULT 0, 
  validation_code Char(255),
  remember_code Char(255),
  admin Bool,
 PRIMARY KEY (userid),
 UNIQUE userid (userid))
;

ALTER TABLE users ADD UNIQUE username (username);
ALTER TABLE users ADD UNIQUE email (email);