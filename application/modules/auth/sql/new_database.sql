/* Clean up OLD database*/
USE user_example;
DROP USER 'user_example'@'localhost' ;
DROP database user_example;


/* Create NEW database*/
Create database user_example; 
USE user_example;
CREATE USER 'user_example'@'localhost' IDENTIFIED BY 'user_example';
GRANT ALL ON user_example.* TO 'user_example'@'localhost';
