
CREATE TABLE users
(
  userid Int NOT NULL AUTO_INCREMENT,
  username Char(20) NOT NULL,
  password Char(255) NOT NULL,
  password_pepper Char(255) NOT NULL,
  email Char(255) NOT NULL,
  email_validated Bool NOT NULL DEFAULT 0,
  new_email Char(255),
  new_email_validated Bool NOT NULL DEFAULT 0, 
  validation_code Char(255),
  remember_code Char(255),
  admin Bool,
 PRIMARY KEY (userid),
 UNIQUE userid (userid))
;

ALTER TABLE users ADD UNIQUE username (username);
ALTER TABLE users ADD UNIQUE email (email);