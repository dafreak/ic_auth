<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Auth Class for user authentication and management
 *
 * @package		IC_Auth
 * @subpackage	Controller
 * @category	Controller
 * @author		IC
 * @version		10-13-2012 23:15:40
 * 
 */
class Auth extends MX_Controller {

	/**
	 * Constructor
	 */
	function __construct() {
		$this->load->library('auth/ic_auth');

		$this->form_validation->CI = &$this;
		parent::__construct();
	}

	// ------------------------------------------------------------------------

	/**
	 * Main Login page
	 *
	 * Displays the login_view and checks to see if the form has been submitted
	 * if so tries to log user in
	 *
	 * @access	public
	 * @return	NULL
	 */
	public function index() {

		if ($this->ic_auth->is_logged_in()) {
			redirect($this->config->item('default_page_login'));
		}
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|alpha_numeric');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');

		$data['message'] = $this->session->flashdata('message');

		if ($this->form_validation->run($this) == FALSE) {
			$data['page'] = 'login_view';
			$data['title'] = $this->lang->line('title_' . $data['page']);
			$data['hide_cp'] = TRUE;
			$this->load->view('auth/template/template_view', $data);
		} else {
			$this->_login_user($this->security->xss_clean($this->input->post('username')), $this->security->xss_clean($this->input->post('password')), $this->security->xss_clean($this->input->post('remember')), $this->security->xss_clean($this->input->post('redirect_page')));
		}

	}

	// ------------------------------------------------------------------------

	/**
	 * Show the success page
	 *
	 * Loads the sucess view and the sessioned flash vars are used to dislpay info
	 *
	 * @access	public
	 * @return	void
	 */
	public function success() {
		$data['hide_cp'] = TRUE;
		$data['page'] = 'success_view';
		$data['title'] = $this->lang->line('title_' . $this->session->flashdata('type') . '_' . $data['page']);
		$data['message'] = $this->session->flashdata('message');
		if ($data['message'] != NULL) {
			$this->load->view('auth/template/template_view', $data);
		} else {
			$this->ic_auth->show_error_page();
			//print_r($data);  // testing

		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Main signup function
	 *
	 * Loads the signup view and does the form validation depending on what has submitted
	 *
	 * @access	public
	 * @return	void
	 */
	public function signup() {
		if (!$this->config->item('allow_signup')) {
			$this->session->set_flashdata('message', '<font color=red>' . $this->lang->line('message_no_signups') . '</font><br />');
			redirect($this->config->item('default_page_site'));
		}
		$data['hide_cp'] = TRUE;

		// field name, error message, validation rules
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|alpha_numeric|external_callbacks[library,ic_auth,username_unique]|external_callbacks[library,ic_auth,username_allowed]');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|external_callbacks[library,ic_auth,email_unique]|valid_email');
		$this->form_validation->set_rules('email2', 'Email Addrrss Confirmation', 'trim|required|matches[email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|matches[password]');

		if ($this->form_validation->run($this) == FALSE) {
			$data['page'] = 'signup_view';
			$data['title'] = $this->lang->line('title_' . $data['page']);
			$this->load->view('auth/template/template_view', $data);
		} else {
			$data['username'] = $this->security->xss_clean($this->input->post('username'));
			$data['password'] = $this->security->xss_clean($this->input->post('password'));
			$data['email'] = $this->security->xss_clean($this->input->post('email'));
			if ($result = $this->users_model->insert_user($data)) {
				$this->ic_auth->send_email($data['username'], 'email_validation');
				$type = 'signup';
				$this->session->set_flashdata('message', '<span class="notify">' . sprintf($this->lang->line('message_' . $type), $data['email']) . "</span>");
				$this->session->set_flashdata('type', $type);
				redirect($this->config->item('default_page_success'));
			} else {
				$this->ic_auth->show_error_page('error_message_signup');
			}
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Log user in or redirect them
	 *
	 * Validates username and password and checks the result to send the user
	 * to the correct view/page
	 *
	 * @access	private
	 * @param	string $username Submitted username
	 * @param	string $password Sumbmitted password
	 * @param	bool $remember If the user has selected to remember them
	 * @param	string $redirect_page The page that the user will be redirected to after login used for the control panel
	 * @return	void
	 */
	private function _login_user($username, $password, $remember, $redirect_page) {
		$result = $this->users_model->validate($username, $password, $remember);

		// Now we verify the result
		if ($result == 2) {
			// If user has not activated their account
			$data = $this->users_model->get_user_db_data($username);
			$data['hide_cp'] = TRUE;
			$data['page'] = 'not_activated_view';
			$data['title'] = $this->lang->line('title_' . $data['page']);
			$this->load->view('auth/template/template_view', $data);

		} elseif ($result == 1) {
			// If user did validate, Send them to members area
			if ($redirect_page == NULL) {
				$redirect_page = $this->config->item('default_page_members');
			}
			redirect($redirect_page);
		} else {
			// If user did not validate, then show them login page again
			$this->session->set_flashdata('message', '<span class="error">' . $this->lang->line('message_bad_login') . '</span>');
			redirect($this->config->item('default_page_login'));
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Setup the admin user
	 *
	 * Creates the admin user and password and opens the login page
	 *
	 * @access	public
	 * @return	void
	 */
	public function do_setup() {
		$result = $this->users_model->setup();
		$message = NULL;
		if ($result) {
			$message = '<span class="notify">' . $this->lang->line('message_admin_account_created') . '</span>';
		}
		$this->session->set_flashdata('message', $msg);
		redirect($this->config->item('default_page_login'));
	}

	// ------------------------------------------------------------------------

	/**
	 * Logs out user
	 *
	 * Clears the session data and deletes all cookies the redirects to the login page
	 *
	 * @access	public
	 * @return	void
	 */
	public function logout() {
		//Clear the session
		$this->session->sess_destroy();
		$this->session->sess_create();

		//delete the remember me cookies if they exist
		if (get_cookie('userid')) {
			delete_cookie('userid');
		}
		if (get_cookie('remember_code')) {
			delete_cookie('remember_code');
		}
		$this->session->set_flashdata('message', '<span class="notify">' . $this->lang->line('message_user_logged_out') . "</span>");
		redirect($this->config->item('default_page_login'));
	}

	// ------------------------------------------------------------------------

	/**
	 * Validates the email address of a user
	 *
	 * Checks the submitted userid and validation code and displays error page if incorrect
	 *
	 * @access	public
	 * @param 	integer $userid Sumbmitted user it from the URI
	 * @param 	string $validation_code Submitted validation_code from the URI
	 * @return	void
	 */
	public function validate_email($userid, $validation_code) {

		if ($this->users_model->validate_email($userid, $validation_code)) {
			$type = 'email_validated';
			$this->session->set_flashdata('message', '<span class="notify">' . $this->lang->line('message_' . $type) . "</span>");
			$this->session->set_flashdata('type', $type);
			redirect($this->config->item('default_page_login'));
		} else {
			$this->ic_auth->show_error_page('error_message_invalid_code');
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Validates the new email address of a user
	 *
	 * Checks the submitted "new" email address userid and validation code and
	 * displays error page if incorrect
	 *
	 * @access	public
	 * @param 	integer $userid Sumbmitted user it from the URI
	 * @param 	string $validation_code Submitted validation_code from the URI
	 * @return	void
	 */
	public function validate_new_email($userid, $validation_code) {

		if ($this->users_model->validate_new_email($userid, $validation_code)) {
			$type = 'email_validated';
			$this->session->set_flashdata('message', '<span class="notify">' . $this->lang->line('message_' . $type) . "</span>");
			$this->session->set_flashdata('type', $type);
			redirect($this->config->item('default_page_success'));
		} else {
			$this->ic_auth->show_error_page('error_message_invalid_code');
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Resend the activation email
	 *
	 * Resends the activation email to the users registered email address and shows error
	 * if incorrect userid
	 *
	 * @access	public
	 * @param 	integer $userid Sumbmitted user it from the URI
	 * @return	void
	 */
	public function resend_activation($userid) {
		$data = $this->users_model->get_user_db_data($userid);
		$email = $data['email'];

		if ($this->ic_auth->send_email($email, 'email_validation')) {
			$type = 'email_validation_sent';
			$this->session->set_flashdata('message', '<span class="notify">' . sprintf($this->lang->line('message_' . $type), $email) . "</span>");
			$this->session->set_flashdata('type', $type);
			redirect($this->config->item('default_page_login'));
		} else {
			$this->ic_auth->show_error_page('error_message_invalid_email');
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Allows the user to send their usename and password to the registered email address
	 *
	 * User inputs a thier email address and sends a validation email to that address and shows
	 * error if the email is invalid
	 *
	 * @access	public
	 * @return	void
	 */
	public function forgot() {

		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');

		$email = $this->security->xss_clean($this->input->post('email'));

		// check to see if and email address
		if ($this->form_validation->run($this) == FALSE) {

			$data['page'] = 'forgot_view';
			$data['title'] = $this->lang->line('title_' . $data['page']);
			$this->load->view('auth/template/template_view', $data);
		} else {
			if ($this->ic_auth->send_email($email, 'forgot')) {
				$type = 'email_validation_sent';
				$this->session->set_flashdata('message', '<span class="notify">' . sprintf($this->lang->line('message_' . $type), $email) . "</span>");
				$this->session->set_flashdata('type', $type);

				redirect($this->config->item('default_page_success'));
			} else {
				$this->ic_auth->show_error_page('error_message_invalid_email');
			}
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Allows the user to reset thier password
	 *
	 * Displays the reset password view and updates the users password and destroys
	 * any session data
	 *
	 * @access	public
	 * @param 	integer $userid Sumbmitted user it from the URI
	 * @param 	string $validation_code Submitted validation_code from the URI
	 * @return	void
	 */
	public function reset_password($userid = NULL, $validation_code = NULL) {

		// field name, error message, validation rules
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|matches[password]');

		if ($this->form_validation->run($this) == FALSE) {
			$data = $this->users_model->get_user_db_data($userid);
			$data['hide_cp'] = TRUE;
			$data['s_userid'] = $userid;
			$data['s_validation_code'] = $validation_code;

			$data['page'] = 'reset_password_view';
			$data['title'] = $this->lang->line('title_' . $data['page']);
			$this->load->view('auth/template/template_view', $data);

		} else {
			$result = FALSE;
			if ($this->users_model->check_validation_code($this->security->xss_clean($this->input->post('s_userid')), $this->security->xss_clean($this->input->post('s_validation_code')))) {

				$result = $this->users_model->change_password($this->security->xss_clean($this->input->post('s_userid')), $this->security->xss_clean($this->input->post('password')));
			}
			if ($result) {

				$this->session->sess_destroy();
				$this->session->sess_create();

				$type = 'reset_successful';
				$this->session->set_flashdata('message', '<span class="notify">' . $this->lang->line('message_' . $type) . "</span>");
				$this->session->set_flashdata('type', $type);

				//TODO change this to a config test to auto login on reset
				redirect($this->config->item('default_page_success'));

				//Auto login user (left here to show how to auto login the user)
				//$this->_login_user($userinfo['username'],$password  );
			} else {
				$this->ic_auth->show_error_page('error_message_update_password');
			}
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Allows the user to change thier password
	 *
	 * Shows the user the change thier password view and allow them to change their password
	 * if the form can validate the current password
	 *
	 * @access	public
	 * @return	void
	 */
	public function change_password() {
		// Force redirect if the user is not logged in
		$this->ic_auth->check_is_logged_in();

		// field name, error message, validation rules
		$this->form_validation->set_rules('current_password', 'Password', 'trim|required|min_length[4]|max_length[32]|external_callbacks[library,ic_auth,password_valid]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|matches[password]');

		if ($this->form_validation->run($this) == FALSE) {
			$user_db_data = $this->users_model->get_user_db_data($this->ic_auth->get_logged_in_userid());
			if ($user_db_data == NULL) {
				$this->ic_auth->show_error_page('error_message_invalid_user');
			} else {
				$data = $user_db_data;
				$data['page'] = 'change_password_view';
				$data['title'] = $this->lang->line('title_' . $data['page']);
				$this->load->view('auth/template/template_view', $data);
			}
		} else {
			// process the request and load the success
			if ($this->users_model->change_password($this->ic_auth->get_logged_in_userid(), $this->security->xss_clean($this->input->post('password')))) {
				$type = 'user_password_updated';
				$this->session->set_flashdata('message', '<span class="notify">' . $this->lang->line('message_' . $type) . "</span>");
				$this->session->set_flashdata('type', $type);

				redirect($this->config->item('default_page_success'));
			} else {
				$this->ic_auth->show_error_page('error_message_update_password');
			}
		}

	}

	// ------------------------------------------------------------------------

	/**
	 * Allows the user to change their email
	 *
	 * Shows the user the change their email view and allow them to change their email
	 *
	 * @access	public
	 * @return	void
	 */
	public function change_email() {
		$this->ic_auth->check_is_logged_in();

		// field name, error message, validation rules
		$this->form_validation->set_rules('new_email', 'Email Address', 'trim|required|valid_email|external_callbacks[library,ic_auth,email_unique]');
		$this->form_validation->set_rules('new_email2', 'Email Addrrss Confirmation', 'trim|required|matches[new_email]');

		if ($this->form_validation->run($this) == FALSE) {
			$user_db_data = $this->users_model->get_user_db_data($this->ic_auth->get_logged_in_userid());
			if ($user_db_data == NULL) {
				$this->ic_auth->show_error_page('error_message_invalid_user');
			} else {
				$data = $user_db_data;
				$data['page'] = 'change_email_view';
				$data['title'] = $this->lang->line('title_' . $data['page']);
				$this->load->view('auth/template/template_view', $data);
			}
		} else {
			// process the request and load the success
			if ($this->users_model->update_new_email($this->ic_auth->get_logged_in_userid(), $this->security->xss_clean($this->input->post('new_email'))) && $this->ic_auth->send_email($this->ic_auth->get_logged_in_userid(), 'new_email_validation')) {
				$type = 'email_validation_sent';
				$this->session->set_flashdata('message', '<span class="notify">' . sprintf($this->lang->line('message_' . $type), $this->security->xss_clean($this->input->post('new_email'))) . "</span>");
				$this->session->set_flashdata('type', $type);

				redirect($this->config->item('default_page_success'));
			} else {
				$this->ic_auth->show_error_page('error_message_update_email');
			}
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Loads the user Profile
	 *
	 * Gets the Currnet userid and loads the profile page
	 *
	 * @access  public
	 * @return  void
	 */
	public function profile() {
		$this->ic_auth->check_is_logged_in();
		$userid = $this->ic_auth->get_logged_in_userid();
		$user_db_data = $this->users_model->get_user_db_data($userid);
		$data = $user_db_data;
		$data['message'] = $this->session->flashdata('message');
		$data['page'] = 'profile_view';
		$data['title'] = $this->lang->line('title_' . $data['page']);
		$this->load->view('auth/template/template_view', $data);
	}

	// ------------------------------------------------------------------------

	 /**
	 * Check for Username for AJAX functions (later use)
	 *
	 * Checks to see if the passed username is not in the database and
	 * echo a message for form validation if it is.
	 *
	 * @access  public
	 * @param   string $key The username to check
	 * @return  void 
	 */
	public function check_username_unique($key) {
		if ($this->users_model->username_exists($key)) {
			echo "FALSE";

		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Check for Email address for AJAX functions (later use)
	 *
	 * Checks to see if the passed email is not in the database and
	 * echo a message for form validation if it is.
	 *
	 * @access  public
	 * @param   string $key the email address to check
	 * @return  bool
	 */
	public function check_email_unique($key) {

		if ($this->users_model->email_exists($key)) {
			echo "FALSE";
		}
	}

	// ------------------------------------------------------------------------

}

// END Auth class

/* End of file auth.php */
/* Location: ./application/modules/auth/controllers/auth.php */
