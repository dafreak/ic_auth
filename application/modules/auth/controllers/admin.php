<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Admin Class for user management
 *
 * @package		IC_Auth
 * @subpackage	Controller
 * @category	Controller
 * @author		IC
 * @version		10-13-2012 22:20:35
 * 
 */
class Admin extends MX_Controller {
	
	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();

		$this->load->library('auth/ic_auth');
		$this->form_validation->CI = &$this;
		// checks the user session and redirects them if required
		$this->ic_auth->check_is_logged_in();
		$this->ic_auth->check_is_admin();
	}

	// ------------------------------------------------------------------------

	/**
	 * Index page for the Admin section
	 *
	 * Displays the admin_home_view
	 *
	 * @access  public
	 * @return  NULL
	 */
	 public function index() {
		$data['page'] = 'admin_home_view';
		$data['title'] = $this->lang->line('title_' . $data['page']);
		$data['message'] = $this->session->flashdata('message');
		$this->load->view('auth/admin/template/template_view', $data);
	}

	// ------------------------------------------------------------------------


	/**
	 * User Management 
	 *
	 * Shows a list of users and give management options
	 * note: using HMVC means you have to set the uri_segment
	 * to ensure the right value is used which is passed via
	 * $offset
	 *
	 * @access  public
	 * @param 	integer $offset Offset of the query set via URI
	 * @return  NULL
	 */
	public function user_management($offset = 0) {

		$this->load->library('pagination');

		$limit = 25;

		$query = $this->users_model->get_users($limit, $offset);
		$all_query = $this->users_model->get_users();

		$config['base_url'] = base_url() . 'auth/admin/user_management/';
		$config['total_rows'] = $all_query->num_rows();

		$config['per_page'] = $limit;
		$config['uri_segment'] = 4;
		$config['num_links'] = 5;
		
		
		$data['users'] = $query->result();
		$data['page'] = 'admin_control_panel_view';
		$data['title'] = $this->lang->line('title_' . $data['page']);
		$data['message'] = $this->session->flashdata('message');

		$this->pagination->initialize($config);

		$data['pagination_links'] = $this->pagination->create_links();
		$this->load->view('auth/admin/template/template_view', $data);
	}

	// ------------------------------------------------------------------------

	/**
	 * Add user to the database
	 *
	 * Loads the add user view and does the form validation depending on what is submitted
	 *
	 * @access   public
	 * @return   void
	 */
	public function add_user() {

		// field name, error message, validation rules
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|alpha_numeric|external_callbacks[library,ic_auth,username_unique]|external_callbacks[library,ic_auth,username_allowed]');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|external_callbacks[library,ic_auth,email_unique]|valid_email');
		$this->form_validation->set_rules('email2', 'Email Addrrss Confirmation', 'trim|required|matches[email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|matches[password]');

		if ($this->form_validation->run($this) == FALSE) {
			$data['page'] = 'admin_add_user_view';
			$data['title'] = $this->lang->line('title_' . $data['page']);
			$this->load->view('admin/template/template_view', $data);
		} else {
			$data['username'] = $this->security->xss_clean($this->input->post('username'));
			$data['password'] = $this->security->xss_clean($this->input->post('password'));
			$data['email'] = $this->security->xss_clean($this->input->post('email'));
			$data['admin'] = $this->security->xss_clean($this->input->post('admin'));
			$data['email_validated'] = $this->security->xss_clean($this->input->post('email_validated'));

			if ($result = $this->users_model->insert_user($data)) {

				if ($this->security->xss_clean($this->input->post('send_email_validation')) == '1') {
					$this->ic_auth->send_email($data['username'], 'email_validation');
				}
				$this->form_validation->unset_field_data();
				
				$data['message'] = '<span class="notify">' . $this->lang->line('message_user_account_created') . '</span>';
				$data['page'] = 'admin_add_user_view';
				$data['title'] = $this->lang->line('title_' . $data['page']);
				$this->load->view('admin/template/template_view', $data);

			} else {
				$this->ic_auth->show_error_page('error_message_add_user');

			}
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Edit a User
	 *
	 * Loads the edit views and does the form validation depending on what is submitted
	 *
	 * @access   public
	 * @return   void
	 */
	public function edit_user() {

		// field name, error message, validation rules
		$this->form_validation->set_rules('email', 'Email Address', 'trim|valid_email');
		$this->form_validation->set_rules('email2', 'Email Addrrss Confirmation', 'trim|matches[email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|matches[password]');

		$userid = $this->uri->segment(4,$this->security->xss_clean($this->input->post('userid')));

		$data['user'] = $this->users_model->get_user_db_data($userid);
		if ($this->form_validation->run($this) == FALSE) {
			if ($data['user'] != NULL) {
				$data['page'] = 'admin_edit_user_view';
				$data['title'] = $this->lang->line('title_' . $data['page']);
				$this->load->view('admin/template/template_view', $data);
			} else {
				$this->ic_auth->show_error_page('error_message_invalid_user');
			}
		} else {
			$new_password = $this->security->xss_clean($this->input->post('password'));
			$new_email = $this->security->xss_clean($this->input->post('email'));
			
			$update_data = array();
			$email_changed = FALSE;
			$update_data['userid'] = $userid;
			
			if ($new_password != NULL) {
				$update_data['password'] = $new_password;
			}
			if ($new_email != $data['user']['email']) {
				$email_changed = TRUE;
				$update_data['email'] = $new_email;
			}
			$update_data['admin'] = $this->security->xss_clean($this->input->post('admin'));
			$update_data['email_validated'] = $this->security->xss_clean($this->input->post('email_validated'));

			if ($result = $this->users_model->update_user($update_data)) {

				if ($this->security->xss_clean($this->input->post('send_email_validation')) == '1') {
					$this->ic_auth->send_email($data['user']['username'], 'email_validation');
				}

				//get updated data
				$data['user'] = $this->users_model->get_user_db_data($userid);
				$data['message'] = '<span class="notify">' . $this->lang->line('message_user_account_updated') . '</span>';
				$data['page'] = 'admin_edit_user_view';
				$data['title'] = $this->lang->line('title_' . $data['page']);
				$this->load->view('admin/template/template_view', $data);
			} else {
				$this->ic_auth->show_error_page('error_message_edit_user');
			}
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Delete a User
	 *
	 * deletes the user and then redirect to the user_management page
	 *
	 * @access   public
	 * @return   void
	 */
	public function delete_user($userid) {
		$this->users_model->delete_user($userid);
		$this->session->set_flashdata('message', '<span class="notify">' . $this->lang->line('message_user_account_delete') . '</span>');
		redirect($this->config->item('default_page_admin_user_management'));
	}

	// ------------------------------------------------------------------------

}

// END Admin class

/* End of file admin.php */
/* Location: ./application/module/auth/controller/admin.php */