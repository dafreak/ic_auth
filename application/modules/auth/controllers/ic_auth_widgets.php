<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Auth Class for user authentication
 *
 * @package     IC_Auth
 * @subpackage  Controller
 * @category    Controller
 * @author      IC
 * @version		10-13-2012 22:18:42
 */
class Ic_auth_widgets extends MX_Controller {

	/**
	 * Constructor
	 */
	function __construct() {
		$this->load->library('auth/ic_auth');
		parent::__construct();
	}

	// ------------------------------------------------------------------------

	/**
	 * Remaps all direct requests to a 404 error
	 *
	 * Displays 404 error if not called from modules::run
	 *
	 * @access  public
	 * @return  NULL
	 */
	public function _remap() {
		show_404();
	}

	// ------------------------------------------------------------------------

	/**
	 * Loads the control panel widget
	 *
	 * Displays user or login view widget depending on if the user is logged in
	 *
	 * @access  public
	 * @param	bool $hide_cp If TRUE then the control panel view will not be loaded
	 * @param	string $redirect_page Passes the page for redirection to the login view
	 * @return  NULL
	 */

	public function cp($hide_cp, $redirect_page) {
		if (!$hide_cp) {
			if ($this->ic_auth->is_logged_in()) {
				$user_db_data = $this->users_model->get_user_db_data($this->ic_auth->get_logged_in_userid());
				$this->load->view('auth/widgets/user_cp_widget', $user_db_data);
			} else {
				$this->load->view('auth/widgets/login_cp_widget', array('redirect_page' => $redirect_page));
			}
		}
	}
	
	// ------------------------------------------------------------------------
}

// END Ic_auth_widget class

/* End of file Ic_auth_widget.php */
/* Location: ./application/modules/auth/controllers/ic_auth_widget.php */