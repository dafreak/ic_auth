<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Admin Class for user management
 *
 * @package		IC_Auth
 * @subpackage	Libraries
 * @category	Libraries
 * @author		IC
 * @version		10-14-2012 00:30:33
 *
 */
class Ic_auth {

	/**
	 * Constructor
	 */
	function __construct() {
		// load required librarys/config/language/models and helpers
		$this->load->library('form_validation');
		$this->load->config('auth/ic_auth');
		$this->load->language('auth/ic_auth');
		$this->load->model('auth/users_model');
		$this->load->helper('cookie');
	}

	/**
	 * __get
	 *
	 * Enables the use of CI super-global without having to define an extra variable.
	 *
	 * @access	public
	 * @param	mixed $var
	 * @return	mixed
	 */
	public function __get($var) {
		return     get_instance()->$var;
	}

	// ------------------------------------------------------------------------

	/**
	 * Shows the error page
	 *
	 * Loads up string form the language file into flash variables and then
	 * redirects to the error page this uses the error module.
	 *
	 * @access	public
	 * @param	string $key The language key for the language file look up
	 * @return	void
	 */
	public function show_error_page($key) {
		// Handle the show error request nusting the error module
		$this->session->set_flashdata('error_title', $this->lang->line('title_' . $key));
		$this->session->set_flashdata('error_message', $this->lang->line($key));
		redirect($this->config->item('default_page_error'));

	}

	/**
	 * Checks to see if the user is an admin
	 *
	 * Looks up the session data and sends the user to an error page if they
	 * are not an admin
	 *
	 * @access   public
	 * @return   void
	 */
	public function check_is_admin() {
		if (!$this->session->userdata('admin') == 1) {
			$this->show_error_page('error_message_unauthorized_access');
		}
	}

	// -----------------------------------------------------------------------

	/**
	 * Sends email
	 *
	 * Sends an email to the users registered email address and selects the template
	 *
	 * @access  public
	 * @param   mixed $usersearch The values of what to look up in the users table
	 * @param   string $type The type of email template that is used
	 * @return  bool
	 */
	public function send_email($usersearch, $type = NULL) {
		$user_db_data = $this->users_model->get_user_db_data($usersearch);
		if ($user_db_data != NULL) {
			$to = $user_db_data['email'];

			switch ($type) {
				case 'email_validation' :
					$template = 'auth/email_templates/validate_email';
					break;
				case 'forgot' :
					$template = 'auth/email_templates/forgot';
					break;
				case 'new_email_validation' :
					$template = 'auth/email_templates/validate_new_email';
					$to = $user_db_data['new_email'];
					break;
				default :
					$template = NULL;
					break;
			}

			$subject = $this->lang->line('email_subject_' . $type);
			$message = $this->load->view($template, $user_db_data, true);
			$from = $this->config->item('email_from');
			$from_name = $this->config->item('email_from_name');

			// To send HTML mail, the Content-type header must be set
			$headers = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

			// Additional headers
			$headers .= "To: " . $user_db_data['username'] . " <$to>\r\n";
			$headers .= "From: $from_name <$from>\r\n";

			mail($to, $subject, $message, $headers);
			return TRUE;
		} else {
			return FALSE;
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Checks to see if the user is logged in and redirects if not
	 *
	 * Checks to see if the user session has been created and redirects the user
	 * if not. this is simple helper function for controlllers
	 *
	 * @access   public
	 * @param    string $redirect_url set if required
	 * @return   bool
	 */
	public function check_is_logged_in($redirect_url = NULL) {
		if ($redirect_url == NULL) {
			$redirect_url = $this->config->item('default_page_login');
		}
		if (!$this->is_logged_in()) {
			redirect($redirect_url);
		} else {
			return TRUE;
		}
	}

	// -----------------------------------------------------------------------

	/**
	 * Gets the userid from the session data
	 *
	 * returns the currently logged in userid from the session data
	 *
	 * @access   public
	 * @return   int
	 */
	public function get_logged_in_userid() {

		return $this->session->userdata('userid');
	}

	// -----------------------------------------------------------------------

	/**
	 * Gets the username from the session data
	 *
	 * returns the currently logged in username from the session data
	 *
	 * @access   public
	 * @return   string
	 */
	public function get_logged_in_username() {

		return $this->session->userdata('username');
	}

	// -----------------------------------------------------------------------

	/**
	 * Checks to see if the user is logged in
	 *
	 * Tries to login the user using the cookies then checks the session data returns bool
	 *
	 * @access   public
	 * @return   bool
	 */
	public function is_logged_in() {
		$this->users_model->login_remembered_user();
		if (!$this->session->userdata('logged_in')) {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Callback Check for Username for filtered words for from validation
	 *
	 * Checks to see if the passed username does not have any of that filtered
	 * words.
	 *
	 * @access  public
	 * @param   string $key the item to check for in the users table
	 * @return  bool
	 */
	public function username_allowed($key) {
		if (!$this->config->item('filter_username')){
			return TRUE;
		}
		$matches = array();
		$matchFound = preg_match_all("/(" . implode($this->config->item('filter_words'), "|") . ")/i", $key, $matches);

		if ($matchFound) {
			 $this->form_validation->set_message('external_callbacks', $this->lang->line('message_username_not_allowed'));
			return FALSE;
		} else {
			return TRUE;
		}

	}

	/**
	 * Callback Check for Username for from validation
	 *
	 * Checks to see if the passed username is not in the database and
	 * sets a message for form validation if it is.
	 *
	 * @access  public
	 * @param   string $key the item to check for in the users table
	 * @return  bool
	 */
	public function username_unique($key) {
		if ($this->users_model->username_exists($key)) {
			$this->form_validation->set_message('external_callbacks', $this->lang->line('message_username_already_exists'));
			return FALSE;
		} else {
			return TRUE;
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Callback Check for Email address for from validation
	 *
	 * Checks to see if the passed email is not in the database and
	 * sets a message for form validation if it is.
	 *
	 * @access  public
	 * @param   string $key the item to check for in the users table
	 * @return  bool
	 */
	public function email_unique($key) {
		if ($this->users_model->email_exists($key)) {
			$this->form_validation->set_message('external_callbacks', $this->lang->line('message_email_address_already_exists'));
			return FALSE;
		} else {
			return TRUE;
		}
	}

	// ------------------------------------------------------------------------

	/**
	 * Callback Check for Password for from validation
	 *
	 * Checks to see if the passed password is not in the database and
	 * sets a message for form validation if it is.
	 *
	 * @access  public
	 * @param   string $key the item to check for in the users table
	 * @return  bool
	 */
	public function password_valid($key) {
		if ($this->users_model->check_password( $this->get_logged_in_userid(),$key)) {
			return TRUE;
		} else {
			$this->form_validation->set_message('external_callbacks', $this->lang->line('message_current_password_incorrect'));
			return FALSE;
		}
	}

	// ------------------------------------------------------------------------

}

// END Ic_auth class

/* End of file ic_auth.php */
/* Location: ./application/module/auth/libraries/ic_auth.php */
