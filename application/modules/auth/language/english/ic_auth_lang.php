<?php 
/*
| -------------------------------------------------------------------
| IC_AUTH English Language File
| -------------------------------------------------------------------
| This file will contain the language data for the ic_auth module
|
 */

$ci = get_instance(); // CI_Loader instance 

//Site Info
$lang['site_title']							=$ci->config->item('site_title');

//Page Titles
$lang['title_login_view'] 	  	 			= 'Login to '. $lang['site_title'];
$lang['title_signup_view']					= 'Signup to '. $lang['site_title'];
$lang['title_error_view']					= 'Error';
$lang['title_forgot_view']					= 'Forgot Username/Password';
$lang['title_forgot_sent_view']				= 'Email Sent';
$lang['title_not_activated_view']			= 'Account Not Activated';
$lang['title_change_password_view'] 		= 'Change Password';
$lang['title_change_email_view'] 			= 'Change Email';
$lang['title_profile_view'] 				= 'Update Profile';

// Success View titles and headings
$lang['title__success_view']						= 'Successful';
$lang['title_signup_success_view']					= 'Signup Successful';
$lang['title_email_validate_success_view']			= 'Email Validated';
$lang['title_change_email_success_view'] 			= 'Email Changed Successfully';
$lang['title_change_password_success_view']			= 'Password Changed Successfully';
$lang['title_reset_successful_success_view']		= 'Reset Successful';
$lang['title_email_validation_sent_success_view'] 	= 'Email Sent Successfully';


$lang['title_admin_add_user_view']		= 'Add User';
$lang['title_admin_home_view']			= 'Admin Main Page';
$lang['title_user_management_view']		= 'User management';
$lang['title_admin_edit_user_view']		= 'Edit User';
//
//Messages for display
$lang['message_bad_login']              = 'Invalid username/password';
$lang['message_admin_account_created']  = 'admin user created with default password';
$lang['message_user_logged_out']        = 'You have logged out';
$lang['message_no_signups']             = 'Signups are closed';
$lang['message_user_account_created']   = 'User account created';
$lang['message_user_account_updated']   = 'User account updated';
$lang['message_user_account_delete']    = 'User account deleted';
$lang['message_user_email_updated']		= 'Email address updated';
$lang['message_user_password_updated']	= 'Password updated';
$lang['message_reset_successful']		= 'Password reset successful';
$lang['message_email_validated'] 		= 'Email validated';
$lang['message_email_validation_sent']	= "Validation email sent to %s";
$lang['message_signup']					= "You have successfully signed up please check %s for validation details";

$lang['message_username_not_allowed']			= 'Username not allowed';
$lang['message_current_password_incorrect']		= 'Current password incorrect';
$lang['message_email_address_already_exists']	= 'Email address already registered';
$lang['message_username_already_exists']		= 'Username already registered';

$lang['email_subject_email_validation']			= 'Welcome to  '. $lang['site_title'];
$lang['email_subject_new_email_validation'] 	= 'New Email request for '. $lang['site_title'];
$lang['email_subject_forgot'] 					= 'Account information/Password Reset for ' . $lang['site_title'];
 
 
 //Error Message
$lang['title_error_message_signup']               = 'An error was encountered';
$lang['title_error_message_update_email']         = 'An error was encountered';
$lang['title_error_message_update_password']      = 'An error was encountered';
$lang['title_error_message_invalid_code']         = 'An error was encountered';
$lang['title_error_message_invalid_user']         = 'An error was encountered';
$lang['title_error_message_invalid_email']        = 'An error was encountered';
$lang['title_error__message_unauthorized_access'] = '401 Unauthorized access';
$lang['title_error_message_add_user']             = 'An error was encountered';
$lang['title_error_message_edit_user']            = 'An error was encountered';
$lang['title_error_message_delete_user']          = 'An error was encountered';


$lang['error_message_signup'] 				= 'Unable to signup please go back and try again';
$lang['error_message_update_email'] 		= 'Unable to update email address';
$lang['error_message_update_password']		= 'Unable to update password';
$lang['error_message_invalid_code'] 		= 'Invalid code';
$lang['error_message_invalid_user']			= 'Invalid user';
$lang['error_message_invalid_email']		= 'Invalid email';
$lang['error_message_unauthorized_access']	= 'You do not have permission to access this page';	
$lang['error_message_add_user']             = 'Unable to add user';
$lang['error_message_edit_user']            = 'Unable to edit user';
$lang['error_message_delete_user']          = 'Unable to delete user';
 

/* End of file ic_auth_lang.php */
/* Location: ./application/modules/auth/language/english/ic_auth_lang.php */