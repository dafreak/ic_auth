<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description here
 *
 * @package		IC_Auth
 * @subpackage	Model
 * @category	Model
 * @author		IC
 * @version     10-15-2012 18:33:35
 */

class Users_model extends CI_Model {

    /**
     * Constructor
     */
    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
    }

    // ------------------------------------------------------------------------

    /**
     * Check to see if username exists
     *
     * Searchs to see if there are any matching username entries in
     * the username field of the user table and returns TRUE if any are found
     *
     * @access	public
     * @param	string $username The string for the username used in the query
     * @return	bool
     */
    public function username_exists($username) {
        $this->db->where('username', strtolower($username));
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Check to see if email exists
     *
     * Searchs to see if there are any matching email entries in
     * the email field of the user table and returns TRUE if any are found
     *
     * @access  public
     * @param   string $email The string for the email address used in the query
     * @return  bool
     */
    public function email_exists($email) {
        $this->db->where('email', strtolower($email));
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    // ------------------------------------------------------------------------

    /**
     * Checks the validation code of the user
     *
     * Runs a query on the users table with the submitted userid and validation_code
     * and return TRUE if found
     *
     * @access	public
     * @param	int $userid The userid to use in the query
     * @param	string $validation_code The validation_code to use in the query
     * @return	bool
     */
    public function check_validation_code($userid, $validation_code) {
        $this->db->where('userid', $userid);
        $this->db->where('validation_code', $validation_code);
        $query = $this->db->get('users');
        if ($query->num_rows() == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Checks the password of the user
     *
     * Checks the submitted un hashed password against the password for that submitted userid by
     * hashing the submitted password with the salt and the users pepper returns true if they match
     *
     * @access	public
     * @param	string $password The submitted non hashed password
     * @param	int $userid The submitted userid
     * @return	bool
     */
    public function check_password($userid,$password) {
        $password_salt = $this->config->item('encryption_key');
        $user_db_data = $this->get_user_db_data($userid);

        if ($user_db_data != NULL) {
            $hashed_password = sha1($password . $password_salt . $user_db_data['password_pepper']);
            $this->db->where('password', $hashed_password);
            $this->db->where('userid', $userid);
            $query = $this->db->get('users');
            if ($query->num_rows() > 0) {
                return TRUE;
            }
        }
        return FALSE;
    }

    // ------------------------------------------------------------------------

    /**
     * Validates the user agaist the usertable
     *
     * Checks the sumbitted username and password against the username password in the user table
     * then checks to see if the email has been validated if so creates the session and the remembers
     * the user if required
     *
     * @access	public
     * @param	string $username The submitted username
     * @param	string $password The submitted unhashed password
     * @param	bool $remember Bool to check to see if cookies should be set
     * @return	int 0,1,2 returns depending of they status of the user
     */
    public function validate($username, $password, $remember) {
        $user_db_data = $this->get_user_db_data($username);
        // Let' see if there if the username is in the table
        if ($user_db_data != NULL) {
            // Let's check the submitted password agaisnt users password
            if ($this->check_password($user_db_data['userid'],$password)) {
                // check if the user has vaildated ther email
                if ($user_db_data['email_validated'] == 1) {
                    // If there the passwords match, then create session data
                    $this->create_user_session($user_db_data['userid']);
                    if (strtolower($remember) == 'remember') {
                        $this->remember_user($user_db_data['userid']);
                    }
                    return 1;
                } else {
                    return 2;
                }
            }
        }
        // If the previous process did not validate then return FALSE.
        return 0;
    }

    // ------------------------------------------------------------------------

    /**
     * Creates the admin user
     *
     * Inserts the admin user to the user table based on values in the auth config
     *
     * @access	public
     * @return	bool
     */
    public function setup() {

        $data = array();
        $data['username'] = $this->config->item('admin_username', 'auth');
        $data['password'] = $this->config->item('admin_password', 'auth');
        $data['email'] = $this->config->item('admin_email', 'auth');
        $data['admin'] = 1;
        $data['email_validated'] = 1;
        return $this->insert_user($data);
    }

    // ------------------------------------------------------------------------

    /**
     * Deletes User from the database
     *
     * Deletes the user from the user table based on userid passed
     *
     * @access  public
     * @param   int $user The userid that is to be deleted
     * @return  bool
     */
    public function delete_user($userid) {
        $this->db->where('userid', $userid);
        return $this->db->delete('users');
    }

    // ------------------------------------------------------------------------

    /**
     * Creates the user
     *
     * Inserts the user to the user table based on values passed and create the
     * validation code aswell as creating the users pepper and then salts and pepper 
     * the password
     *
     * @access	public
     * @param	array $userdata The submitted users information
     * @return	bool
     */
    public function insert_user($userdata) {
        // check the username
        $user_db_data = $this->get_user_db_data($userdata['username']);

        if ($user_db_data == NULL) {
            $password_salt = $this->config->item('encryption_key');
            $password_pepper = $this->random_sha1();
            $email_vaildation_code = $this->random_sha1();

            $data = array();
            $data['username'] = strtolower($userdata['username']);
            $data['password'] = sha1($userdata['password'] . $password_salt . $password_pepper);
            $data['password_pepper'] = $password_pepper;
            $data['email'] = $userdata['email'];
            $data['validation_code'] = $email_vaildation_code;

            if (isset($userdata['email_validated']) && $userdata['email_validated'] != NULL) {
                $data['email_validated'] = $userdata['email_validated'];
            } else {
                $data['email_validated'] = 0;
            }

            if (isset($userdata['admin']) && $userdata['admin'] != NULL) {
                $data['admin'] = $userdata['admin'];
            } else {
                $data['admin'] = 0;
            }

            $this->db->insert('users', $data);
            return TRUE;
        }
        return FALSE;
    }

    // ------------------------------------------------------------------------

    /**
     * Updates the users information
     *
     * Updates the users information based on the submitted data and creates
     *
     * @access   public
     * @param    array $userdata The users information 
     * @return   bool
     */
    public function update_user($userdata) {
        $data = array();
        if (isset($userdata['password']) && $userdata['password'] != NULL) {
            $this->change_password($userdata['userid'], $userdata['password']);
        }
        if (isset($userdata['email']) && $userdata['email'] != NULL) {
            $data['email'] = $userdata['email'];
        }

        if (isset($userdata['email_validated']) && $userdata['email_validated'] != NULL) {
            $data['email_validated'] = $userdata['email_validated'];
        } else {
            $data['email_validated'] = 0;
        }

        if (isset($userdata['admin']) && $userdata['admin'] != NULL) {
            $data['admin'] = $userdata['admin'];
        } else {
            $data['admin'] = 0;
        }
        $this->db->where('userid', $userdata['userid']);
        return $this->db->update('users', $data);

    }

    // ------------------------------------------------------------------------

    /**
     * Updates email_validation code
     *
     * Finds the userid and validation code and sets the email_validated field to 1 
     * then changes the validation code
     *
     * @access	public
     * @param	int $userid The userid that is used in the query
     * @param	string $validation_code The validation_code that is used in the query
     * @return	bool
     */
    public function validate_email($userid, $validation_code) {

        // Prep the query
        $this->db->where('userid', $userid);
        $this->db->where('validation_code', $validation_code);

        // Run the query
        $query = $this->db->get('users');

        // Let' see if there if the username is in the table
        if ($query->num_rows == 1) {
            $data = array('email_validated' => 1);

            $this->db->where('userid', $userid);
            $this->db->update('users', $data);

            $this->change_validation_code($userid);
            return TRUE;
        }
        return FALSE;
    }

    // ------------------------------------------------------------------------

    /**
     * Updates the users email address with new_email address
     *
     * Finds the userid and validation code and sets the new_email_validated field to 1
     * and replace the email with new_email then clears the new_email field
     *
     * @access	public
     * @param   int $userid The userid that is used in the query
     * @param   string $validation_code The validation_code that is used in the query
     * @return	bool
     */
    public function validate_new_email($userid, $validation_code) {

        $this->db->where('userid', $userid);
        $this->db->where('validation_code', $validation_code);

        // Run the query
        $query = $this->db->get('users');

        // Let' see if there if the username is in the table
        if ($query->num_rows == 1) {
            $user_db_data = $query->row_array();
            if (!$this->email_exists($user_db_data['new_email'])) {
                $data['new_email_validated'] = 1;
                $data['email'] = $user_db_data['new_email'];
                $data['new_email'] = "";

                $this->db->where('userid', $userid);
                $this->db->update('users', $data);

                $this->change_validation_code($userid);
                return TRUE;
            }
        }
        return FALSE;
    }

    // ------------------------------------------------------------------------

    /**
     * Creates a random sha1 code
     *
     * Creates random string then applies the sha1 hash to it
     *
     * @access	private
     * @return	string
     */
    private function random_sha1() {

        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, strlen($alphabet) - 1);
            $pass[$i] = $alphabet[$n];
        }
        //turn the array into a string and return the sha1 hash
        return sha1(implode($pass));
    }

    // ------------------------------------------------------------------------

    /**
     * Changes the users validation code
     *
     * Updates the users validation_code witha random sha1 string
     *
     * @access	private
     * @param	int $userid The submitted userid 
     * @return	bool
     */
    private function change_validation_code($userid) {
        $user_db_data = $this->get_user_db_data($userid);
        // Let' see if there if the username is in the table
        if ($user_db_data != NULL) {
            $validation_code = $this->random_sha1();

            $data['validation_code'] = $validation_code;
            $this->db->where('userid', $userid);
            $this->db->update('users', $data);
            return TRUE;
        }
        return FALSE;
    }

    // ------------------------------------------------------------------------

    /**
     * Gets the users database row data
     *
     * Searchs the user table based on the type of param passed first checks
     * the username then email then finally the userid and return the row
     * as an array
     *
     * @access	public
     * @param	mixed $identity The username/email or userid used to look up the users data 
     * @return	array
     */
    public function get_user_db_data($identity) {

        $data = array();
        // first check the username
        $this->db->where('username', strtolower($identity));
        $query = $this->db->get('users');

        if ($query->num_rows == 1) {
            $data = $query->row_array();
        } else {
            $this->db->where('email', strtolower($identity));
            $query = $this->db->get('users');

            if ($query->num_rows == 1) {
                $data = $query->row_array();
            } else {
                $this->db->where('userid', $identity);
                $query = $this->db->get('users');

                if ($query->num_rows == 1) {
                    $data = $query->row_array();
                }
            }
        }
        return $data;
    }

    // ------------------------------------------------------------------------

    /**
     * Changes the users password
     *
     * Creates a random sha1 code for the password pepper then update the users
     * password with the hash of password + salt + pepper and changes the validation_code
     *
     * @access	public
     * @param	int $userid The submitted userid
     * @param	string $password The submitted unhashed password 
     * @return	bool
     */
    public function change_password($userid, $password) {

        $password_salt = $this->config->item('encryption_key');
        $password_pepper = $this->random_sha1();

        $data['password'] = sha1($password . $password_salt . $password_pepper);
        $data['password_pepper'] = $password_pepper;

        $this->db->where('userid', $userid);
        $return = $this->db->update('users', $data);
        $this->change_validation_code($userid);

        return $return;

    }

    // ------------------------------------------------------------------------

    /**
     * Updates the users new_email field
     *
     * Update the user row with the supllied new_email string and changes the
     * validation_code
     *
     * @access	public
     * @param	int $userid The submitted userid
     * @param	string $new_email The submitted new email address to be updated
     * @return	bool
     */
    public function update_new_email($userid, $new_email) {

        $data['new_email'] = $new_email;
        $data['new_email_validated'] = 0;
        $this->db->where('userid', $userid);
        $result = $this->db->update('users', $data);
        $this->change_validation_code($userid);

        return $result;

    }

    // ------------------------------------------------------------------------

    /**
     * Changes the users remember_code
     *
     * Update the user row with the supllied remembe_code string
     *
     * @access	public
     * @param   int $userid The submitted userid
     * @param	string $remembercode the remember_code to be updated 
     * @return	bool
     */
    public function update_remember_code($userid, $remember_code) {
        $data['remember_code'] = $remember_code;
        $this->db->where('userid', $userid);
        $result = $this->db->update('users', $data);
        return $result;
    }

    // ------------------------------------------------------------------------

    /**
     * Remembers user
     *
     * Creates cookies required to remember user
     *
     * @access	public
     * @param   int $userid The submitted userid
     * @return	bool
     */
    public function remember_user($userid) {

        if ($userid == NULL) {
            return FALSE;
        }
        $user_db_data = $this->get_user_db_data($userid);
        $password_salt = $this->config->item('encryption_key');
        $remember_code = sha1("Remember Me" . $password_salt . $user_db_data['password_pepper']);

        if ($this->update_remember_code($userid, $remember_code)) {
            // if the user_expire is set to zero we'll set the expiration two years from now.
            if ($this->config->item('user_expire') === 0) {
                $expire = (60 * 60 * 24 * 365 * 2);
            }
            // otherwise use what is set
            else {
                $expire = $this->config->item('user_expire');
            }

            set_cookie(array('name' => 'userid', 'value' => $user_db_data['userid'], 'expire' => $expire));
            set_cookie(array('name' => 'remember_code', 'value' => $remember_code, 'expire' => $expire));

            return TRUE;
        }

        return FALSE;
    }

    // ------------------------------------------------------------------------

    /**
     * Logs in Remembers user
     *
     * Check the cookies and if they are correct creats the user session
     *
     * @access	public
     * @return	bool
     */
    public function login_remembered_user() {

        //check for valid data
        if (!get_cookie('userid') || !get_cookie('remember_code') || !$this->get_user_db_data(get_cookie('userid'))) {
            return FALSE;
        }

        $user_db_data = $this->get_user_db_data(get_cookie('userid'));
        if ($user_db_data != NULL) {
            $cookie_remember_code = get_cookie('remember_code');
            if ($user_db_data['remember_code'] == $cookie_remember_code) {
                $this->create_user_session($user_db_data['userid']);
                return TRUE;
            }
        }
        return FALSE;
    }

    // ------------------------------------------------------------------------

    /**
     * Creates the user session
     *
     * Creates the user session for authentication bases on the user row
     *
     * @access	public
     * @param   int $userid The submitted userid
     * @return	void
     */
    public function create_user_session($userid) {
        $user_db_data = $this->get_user_db_data($userid);

        $session_data['userid'] = $user_db_data['userid'];
        $session_data['email'] = $user_db_data['email'];
        $session_data['username'] = strtolower($user_db_data['username']);
        $session_data['logged_in'] = TRUE;
        $session_data['admin'] = $user_db_data['admin'];

        $this->session->set_userdata($session_data);
    }

    // ------------------------------------------------------------------------

    /**
     * Get all users
     *
     * returns the query on the users table can set limits for pageation
     *
     * @access  public
     * @param	int $limit The limit what the query will return
     * @param	int $offset The offset or count first record the query will return
     * @return  array (the raw query)
     */
    public function get_users($limit = NULL, $offset = NULL) {
        $query = $this->db->get('users', $limit, $offset);
        return $query;
    }

    // ------------------------------------------------------------------------

}

// END user_model Class

/* End of file users_model.php */
/* Location: ./application/module/auth/model/users_model.php */
