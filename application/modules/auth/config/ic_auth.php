<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| IC_AUTH SETTINGS 
| -------------------------------------------------------------------
| This file will contain the settings needed to use the Auth module
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
| Auth uses a basic users table with includes password pepper field 
| for providing a per user pepper. The class uses the encryption_key 
| in the config (application/config/config.php) for the password salt 
| which needs to be changed for per install as it will provide greater 
| security 
| 
| For random Salt try https://api.wordpress.org/secret-key/1.1/salt/ 
|
| Rebuild Databas           application/sql/rebuild_user_example.sql
| Basic Database Setup      application/sql/new_database.sql
| Basic USERS Table SQL     application/sql/users.sql
|
| To access values stored in this config
| $this->config->item('<key_name>'); or
|
*/

/*   
|--------------------------------------------------------------------------
| Site settions
|--------------------------------------------------------------------------
|
| Used to set parameters for the site
|
*/
$config['allow_signup']		= TRUE;
$config['filter_username']	= TRUE;
$config['filter_words']=array('admin','administrator','other naughty words go here');
/*   
|--------------------------------------------------------------------------
| Default pages for redirection
|--------------------------------------------------------------------------
|
| Used to set redirection pages
|
*/

$config['default_page_members']   	= 'site';
$config['default_page_home']      	= 'site';
$config['default_page_login']    	= 'auth';
$config['default_page_signup']    	= 'auth/signup';
$config['default_page_error']     	= 'error';
$config['default_page_admin_cp']    = 'auth/admin';
$config['default_page_success']     = 'auth/success';
$config['default_page_admin_user_management']     =  'auth/admin/user_management';

/* 
|--------------------------------------------------------------------------
| Admin user defaults
|--------------------------------------------------------------------------
|
| Used to set parameters for the admin accoutn creation
|
*/
$config['admin_username']		= 'admin';
$config['admin_password']		= 'password';
$config['admin_email']			= 'admin@localhost.com';

/*	
|--------------------------------------------------------------------------
| Site Infomation
|--------------------------------------------------------------------------
|
| Used to set parameters for the site
|
*/

$config['user_expire']          = 7776000; 	//(90 days)	 How long to remember the user (seconds). Set to zero for no expiration 
$config['email_from']			= 'administrator@no-reply.com';
$config['email_from_name'] 		= 'Administrator';

/* End of file ic_auth.php */
/* Location: ./application/modules/auth/config/ic_auth.php */